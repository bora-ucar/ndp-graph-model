#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

unsigned int scc(unsigned int* Aptrs, unsigned int* Aids, unsigned int n, unsigned int* perm, unsigned int* cptrs, int* lowl, int* spos, unsigned int* prev, unsigned int* tedges);


void constructDGraph(unsigned int *hedges, unsigned int nhedges, unsigned int d, unsigned int n,
	unsigned int *ptrs, unsigned int *vids)
{
	unsigned int i, j, t, src, trt, offset, begOffset;
	unsigned int *marker = (unsigned int *) malloc(sizeof(unsigned int) * (n+1));

	for (i = 0; i <= n; i++)
		ptrs[i] = marker[i] = 0;

	for (i = 0; i < nhedges; i++)
	{
		src = hedges[i * d]; 
		ptrs[src] += d-1;
	}

	for (i = 1; i <= n; i++)
		ptrs[i] += ptrs[i-1];

	if (ptrs[n] != nhedges * (d-1))/*just a sanity check*/
	{
		printf("not the same num hedges,  wrote %d but = %d\n", ptrs[n], nhedges * (d-1));
		exit(1);
	}
	/*we first write all, with duplicates then get rid of them in a pass over the constructed lists*/
	for (i = 0; i < nhedges; i++)
	{
		src = hedges[i * d]; /*the first vertex in each hedge*/
		for (t = 1; t < d; t++)
		{ 
			trt = hedges[i * d + t];
			vids[--ptrs[src]] = trt;
		}
	}
	offset = 0;
	for (i = 0; i < n; i++)
	{		
		begOffset = offset;
		for (j = ptrs[i]; j < ptrs[i+1]; j++)
		{
			trt = vids[j];
			if (trt != i && marker[trt] != i+1)/*no self loops && no parallel edges*/
			{
				vids[offset++] = trt;
				marker[trt] = i+1;
			}
		}
		ptrs[i] = begOffset;
	}
	ptrs[n] = offset;

	free(marker);
}

unsigned int sccHypergraphs(unsigned int *hedges, unsigned int nhedges, unsigned int d, unsigned int *dimensions, unsigned *nedgesGraph)
{
	unsigned int i;
	unsigned int num;
	unsigned int n = dimensions[0]; /*we assume that all dimensions are of size n */

	for (i = 1; i < d; i++)/*just a sanity check*/
	{	
		if(dimensions[i] != n)
		{
			printf("not the same dim dim[%d] = %d but n = %d\n", i, dimensions[i], n );
			exit(1);
		}
	}
	unsigned int *ptrs = (unsigned int *) malloc(sizeof(unsigned int) * (n+1));
	unsigned int *vids = (unsigned int *) malloc(sizeof(unsigned int) * (nhedges * (d-1)));
	unsigned int *perm = (unsigned int *) malloc(sizeof(unsigned int) * n);
	unsigned int *cptrs = (unsigned int *) malloc(sizeof(unsigned int) * n);
	int *lowl = (int *) malloc(sizeof(int) * (n+1));
	int *spos = (int *) malloc(sizeof(int) * (n+1));
	unsigned int *prev = (unsigned int *) malloc(sizeof(unsigned int) * n);
	unsigned int *tedges = (unsigned int *) malloc(sizeof(unsigned int) * (n+1));

	/* 1. Build the directed graph of hedges*/	
	constructDGraph(hedges, nhedges, d, n, ptrs, vids);

	*nedgesGraph =  ptrs[n];
	/* 2. return the num scc of the graph*/
	num = scc(ptrs, vids, n, perm, cptrs, lowl, spos, prev, tedges) ;

	free(tedges);
	free(prev);
	free(spos); 
	free(lowl); 
	free(cptrs); 
	free(perm);	
	free(vids);
	free(ptrs);
	return num;
}

unsigned int scc(unsigned int* Aptrs, unsigned int* Aids, unsigned int n, unsigned int* perm, unsigned int* cptrs, int* lowl, int* spos, unsigned int* prev, unsigned int* tedges) 
{
/*
 * Kamer Kaya: This is an implementation 
 *  of Tarjan's single pass (DFS based) SCC algorithm.
 *
 *
 * 
 * A: matrix, n is the dimension
 * Output:
 * perm: permutation for the BTF
 * cptrs: component pointers in perm
 * num: number of components
 * Work:
 * 	lowl: smallest stack position of any vertex to which a path from node i exists.
 * 	spos: stack position of a vertex
 * 	prev: the vertex at the end of the path
 * 	tedges: number of edges that needs to be searched
 */
	
	/* 
	Bora Ucar: update for unsigned int; 
	*/

	unsigned int i, control, ptr, i2, iv, iw, ist, num, j, lcnt, stp, icnt, tnm;
	if(2 * (n-1) >= INT_MAX)
	{
		printf("n %d is probably too large\n", n );
		exit(1);
	}
	control = icnt = num = 0;
	tnm = 2*n - 1;

	for(i = 0; i < n; i++)
		spos[i] = -1;

	memcpy(tedges, Aptrs, sizeof(unsigned int) * n) ;

	for(i = 0; i < n; i++) 
	{
		if(spos[i] != -1) continue;

		iv = i;
		ist = 1;

	/* put the node to the stack */
		lowl[iv] = spos[iv] = 0;
		cptrs[n-1] = iv;

	/* the body of this loop either puts a new node to the stack or backtrack */
		for(j = 0; j < tnm; j++) 
		{
			ptr = tedges[iv];

	    /* if there exists an edge to visit */
			if(ptr < Aptrs[iv + 1]) 
			{
				i2 = Aptrs[iv + 1];
				control = 0;

		/* search the edges leaving iv until one enters a new node or all edges are exhausted */
				for(; ptr < i2; ptr++)
				{
					iw = Aids[ptr];
					if(iw < n) 
					{
					/* check if node iw has not been on stack already */
						if(spos[iw] == -1) 
						{
			    /* put a new node to the stack */
							tedges[iv] = ptr + 1;
							prev[iw] = iv;
							iv = iw;

							/*lowl[iv] = spos[iv] = ist = ++ist;*/
							lowl[iv] = spos[iv] = (int) ++ist;

							cptrs[n - ist] = iv;
							control = 1;
							break;
						}

			/* update lowl[iw] if necessary */
						if(lowl[iw] < lowl[iv])
						{
							lowl[iv] = lowl[iw];
						}
					}
				}

				if(control == 1) 
				{ 
					control = 0; 
					continue; 
				}
			}

	    /* is node iv the root of a block */
			if(lowl[iv] >= spos[iv]) 
			{
		/* order the nodes in the block */
				lcnt = icnt;
		/* peel block off the top of the stack starting at the top
		 * and working down to the root of the block */
				for(stp = n - ist; stp < n; stp++) 
				{
					iw = cptrs[stp];
					lowl[iw] = (int) n;
					spos[iw] = (int) icnt++;
					if (iw == iv) {break;}
				}
				ist = (n - 1) - stp;
				cptrs[num++] = lcnt;

		/* are there any nodes left on the stack */
				if(ist == 0) 
				{
		    /* if all the nodes have been ordered */
					if(icnt == n) 
					{
						control = 1;
					}
					break;
				}
			}

	    /* backtrack to previous node on a path */
			iw = iv;
			iv = prev[iv];

	    /* update the value of lowl(iv) if necessary */
			if(lowl[iw] < lowl[iv]) 
			{
				lowl[iv] = lowl[iw];
			}
		}
		if(control == 1) 
		{
			break;
		}
	}

    /* put permutation in the required form */
	for(i = 0; i < n; i++) 
	{
		perm[ spos[i] ] = i;
	}
	cptrs[num] = n;

	return num;
}
