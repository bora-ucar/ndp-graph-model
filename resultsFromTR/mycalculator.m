function vv = mycalculator(n, d, c )
%vv = mycalculator(n, d, c )

vv = zeros(size(c));

for ii=1:length(c)
    cc = c(ii);
    v = zeros(n,1);
    p = (log(n) + cc)/n^(d-1);
    
    for t=1:n-1
        v(t) = nchoosek(n,t) * (1-p)^((t*n^(d-1))-t^d );
    end
    
    vv(ii) = max(1-sum(v), 0);
end

end