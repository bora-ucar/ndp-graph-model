
%%%%% This is for n=50, with different c (not in the report) 

datad2n50 = load('outputRuns_d2_c_2_3_n50.txt');
probd2 = datad2n50(:, 6);

datad4n50 = load('outputRuns_d4_c_2_3_n50.txt');
probd4 = datad4n50(:, 6);

datad8n50 = load('outputRuns_d8_c_2_3_n50.txt');
probd8 = datad8n50(:, 6);

datad16n50 = load('outputRuns_d16_c_2_3_n50.txt');
probd16 = datad16n50(:, 6);

c50 = datad4n50(:, 2);

bnd = exp(-2*exp(-c50));

fig = figure(1);
orient(fig, 'landscape');
p = plot(c50, probd2, 'g.:', c50, probd4, 'r+:', c50, probd8, 'm*:', c50, probd16, 'bo:', c50, bnd, '--k', 'MarkerSize',14);
p(5).LineWidth = 3;

ytickformat('%.1f');
xtickformat('%.1f');


legend('$d=2$', '$d=4$', '$d=8$', '$d=16$', '$e^{-2e^{-c}}$','Location','SouthEast', 'FontSize',24, 'Interpreter','latex');
title('for n=50, with different d (not in the report)')
ax = gca;
ax.FontSize = 18;
ax.XTick = -2:0.5:max(c50);
ax.YTick = 0:0.1:1;
ax.XLim = [-2.05 max(c50)+.05];
ax.YLim = [-0.05 1.05];

hold off;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
datad4n100 = load('outputRuns_d4_c_2_4_n100.txt');
datad4n1000 = load('outputRuns_d4_c_2_4_n1000.txt');

c100 = datad4n100(:, 2);
prob100 = datad4n100(:, 6);

c1000 = datad4n1000(:, 2);
prob1000 = datad4n1000(:, 6);
if any(c1000 ~= c100)
    error('not eq prob space');
end

bnd = exp(-2*exp(-c100));

fig = figure(2);
orient(fig, 'landscape');

p = plot(c100, prob100, 'bx:', c1000, prob1000, 'r+:', c100, bnd, '--k', 'MarkerSize',14);
p(3).LineWidth = 3;
ytickformat('%.1f');
xtickformat('%.1f');
legend('$n=100$', '$n=1000$', '$e^{-2e^{-c}}$', 'Location','SouthEast', 'FontSize',24, 'Interpreter','latex');

ax = gca;
ax.FontSize = 18;
ax.XTick = -2:0.5:4;
ax.YTick = 0:0.1:1;
ax.XLim = [-2.05 4.05];
ax.YLim = [-0.05 1.05];
hold off

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

datad2n100 = load('outputRuns_d2_c_2_4_n100.txt');
probd2 = datad2n100(:, 6);

datad4n100 = load('outputRuns_d4_c_2_4_n100.txt');
probd4 = datad4n100(:, 6);

datad8n100 = load('outputRuns_d8_c_2_4_n100.txt');
probd8 = datad8n100(:, 6);

datad16n100 = load('outputRuns_d16_c_2_4_n100.txt');
probd16 = datad16n100(:, 6);

fig = figure(3);
orient(fig, 'landscape');
p = plot(c100, probd2, 'g.:', c100, probd4, 'r+:', c100, probd8, 'm*:', c100, probd16, 'bo:', c100, bnd, '--k', 'MarkerSize',14);
p(5).LineWidth = 3;
ytickformat('%.1f');
xtickformat('%.1f');


legend('$d=2$', '$d=4$', '$d=8$', '$d=16$', '$e^{-2e^{-c}}$','Location','SouthEast', 'FontSize',24, 'Interpreter','latex');

ax = gca;
ax.FontSize = 18;
ax.XTick = -2:0.5:4;
ax.YTick = 0:0.1:1;
ax.XLim = [-2.05 4.05];
ax.YLim = [-0.05 1.05];

hold off;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% plot empri, union bound, and the bound exp
figure(4);
orient(fig, 'landscape');

datad4c03n50m0 = load('outputRuns_d4_c_2_3_n50.txt');
probd4c03n50m0 = datad4c03n50m0(:, 6);
cd4c03n50m0 = datad4c03n50m0(:, 2);
cutoff = cd4c03n50m0>=1.00 & cd4c03n50m0<=3.00;
probd4c03n50m0 = probd4c03n50m0(cutoff);
cd4c03n50m0 = cd4c03n50m0(cutoff);

myc = cd4c03n50m0;
vv = mycalculator(50, 4, myc);
bnd =  exp(-2*exp(-myc));


p = plot(myc, probd4c03n50m0, 'b.:', myc, vv, 'r+:', myc, bnd, '--k', 'MarkerSize',14);
p(3).LineWidth = 3;
ytickformat('%.1f');
xtickformat('%.1f');

legend('$n=50, d=4$ ', '$1-\sum_{t=1}^{n-1} {n\choose t}(1-p)^{t n^{d-1}-t^d}$', '$e^{-2e^{-c}}$', 'Location','SouthEast', 'FontSize',20, 'Interpreter','latex');
%$1-\sum_{t=1}^{n-1}{\binom{n}{t}} (1-p)^{t n^{d-1}-t^d}$
ax = gca;
ax.FontSize = 18;
ax.XTick = 1.0:0.5:3.0;
ax.YTick = 0:0.1:1;
ax.XLim = [0.95 3.07];
ax.YLim = [-0.05 1.05];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


fig = figure (5);
orient(fig, 'landscape');

datad2n100 = load('outputRuns_d2_c_2_4_n100.txt');
numE2 = datad2n100(:, 7);

datad4n100 = load('outputRuns_d4_c_2_4_n100.txt');
numE4 = datad4n100(:, 7);

datad8n100 = load('outputRuns_d8_c_2_4_n100.txt');
numE8 = datad8n100(:, 7);

datad16n100 = load('outputRuns_d16_c_2_4_n100.txt');
numE16 = datad16n100(:, 7);

c100 = datad2n100(:, 2);
p = plot(c100, numE2, 'g.:', c100, numE4, 'r+:', c100, numE8, 'm*:', c100, numE16,'bo:', 'MarkerSize',12);

ytickformat('%.0f');
xtickformat('%.1f');
legend('$d=2$', '$d=4$', '$d=8$', '$d=16$','Location','NorthWest', 'FontSize',20, 'Interpreter','latex');
ax = gca;
ax.FontSize = 18;
maxE = max([numE2; numE4; numE8; numE16]);

ax.XTick = -2:0.5:max(c100);
ax.YTick = 0:1000:ceil(maxE/1000)*1000;
ax.XLim = [-2.05 max(c100)+.05];
ax.YLim = [-0.05 ceil(maxE/1000)*1000+100];

hold off;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%binomial vs uniform

fig = figure (6);
orient(fig, 'landscape');
datad3c03n50 = load('outputRuns_d3_c3_n50_model1.txt');
probd3c03n50 = datad3c03n50(:, 5);
c03n50 =  datad3c03n50(:, 2);

datad4c03n50 = load('outputRuns_d4_c3_n50_model1.txt');
probd4c03n50 = datad4c03n50(:, 5);
c03n502 =  datad4c03n50(:, 2);
if(any(c03n50~= c03n502))
    error('not the same c');
end

datad3c03n50m0 = load('outputRuns_d3_c_2_3_n50.txt');
probd3c03n50m0 = datad3c03n50m0(:, 6);
cd3c03n50m0 = datad3c03n50m0(:, 2);
cutoff = cd3c03n50m0>=0.00 & cd3c03n50m0<=3.00;
probd3c03n50m0 = probd3c03n50m0(cutoff);
cd3c03n50m0 =cd3c03n50m0(cutoff);

datad4c03n50m0 = load('outputRuns_d4_c_2_3_n50.txt');
probd4c03n50m0 = datad4c03n50m0(:, 6);
cd4c03n50m0 = datad4c03n50m0(:, 2);
cutoff = cd4c03n50m0>=0.00 & cd4c03n50m0<=3.00;
probd4c03n50m0 = probd4c03n50m0(cutoff);
cd4c03n50m0 = cd4c03n50m0(cutoff);

datad4c04n100m0 = load('outputRuns_d4_c_2_4_n100.txt');
probd4c03n100m0 = datad4c04n100m0(:, 6);
cd4c03n100m0 = datad4c04n100m0(:, 2);

cutoff = cd4c03n100m0>=0.00 & cd4c03n100m0<=3.00;
cd4c03n100m0 = cd4c03n100m0(cutoff);
probd4c03n100m0 = probd4c03n100m0(cutoff);

bnd =  exp(-2*exp(-c03n50));
p = plot(c03n50, probd3c03n50, 'g.:', c03n50, probd4c03n50, 'r+:', cd3c03n50m0, probd3c03n50m0, 'm*:', cd4c03n50m0, probd4c03n50m0, 'bx:', cd4c03n100m0, probd4c03n100m0, 'c^', c03n50, bnd, '--k', 'MarkerSize',14);
p(6).LineWidth = 3;
ytickformat('%.1f');
xtickformat('%.1f');

legend('$n=50, d=3$ uniform', '$n=50, d=4$ uniform', '$n=50, d=3$ binomial', '$n=50, d=4$ binomial', '$n=100, d=4$ binomial', '$e^{-2e^{-c}}$', 'Location','SouthEast', 'FontSize',20, 'Interpreter','latex');

ax = gca;
ax.FontSize = 18;
ax.XTick = 0:0.5:3.05;
ax.YTick = 0:0.1:1;
ax.XLim = [-0.05 3.07];
ax.YLim = [-0.05 1.05];

