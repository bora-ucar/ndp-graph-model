#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mmio.h"

unsigned int scc(unsigned int* Aptrs, unsigned int* Aids, unsigned int n, unsigned int* perm, 
	unsigned int* cptrs, int* lowl, int* spos, unsigned int* prev, unsigned int* tedges) ;

void ReadMatrixMarket(char *fname, 
	unsigned int **csc_irn, unsigned int **csc_jcn, unsigned int *__m,
	unsigned int *__n, unsigned int *__ne, int readVals)
{
    /*START: MM stuff. */
	MM_typecode matcode;
	int _m, _n, _ne;
    /*END: MM stuff. */

	FILE *ifp;
	double *val = NULL;
	int *I, *J;
	int *wrk; 
	unsigned int *lcsc_jcn, *lcsc_irn;
	int i, hasValues;

	ifp = (FILE*) fopen(fname, "r");

	if(ifp == NULL)
	{
		printf("could not open file %s\n", fname);
		exit(12);
	}

	if (mm_read_banner(ifp, &matcode) != 0)
	{
		fclose(ifp);
		printf("Could not process Matrix Market banner.\n");
		exit(13);
	}
	if (! mm_is_matrix(matcode) )
	{
		fclose(ifp);
		printf("Sorry, this application does not support non matrices");
		exit(10);
	}
	if( !mm_is_sparse(matcode))
	{
		fclose(ifp);
		printf("Sorry, this application does not support dense matrices");
		exit(14);
	}
	if (mm_is_complex(matcode))
	{
		fclose(ifp);
		printf("In this application, the magnitudes of the complex values are used\n");
	//	exit(15);

	}
	/*read in everything.*/
	mm_read_unsymmetric_sparse_mod(fname, &_m, &_n, &_ne, &val, &I, &J, &hasValues);
	*__m = (unsigned int) _m;
	*__n = (unsigned int) _n;
	*__ne = (unsigned int) _ne;
	
	if(hasValues == 0 && readVals != 0) 
	{
		printf("sorry no values in the file\n");
		fflush(stdout);
		exit(12);
	}
	printf("matrix is read in unsymm mod %d %d %d\n", *__m, *__n, *__ne);
	lcsc_jcn = *csc_jcn = (unsigned int *) malloc(sizeof(unsigned int) * ((*__n)+1));
	lcsc_irn = *csc_irn = (unsigned int *) malloc(sizeof(unsigned int) * (*__ne));

	printf("within this file %s we always allocate values\n", __FILE__ );

	wrk = (int *) malloc(sizeof(int ) * ((*__n)+1));
	memset(wrk, 0, sizeof(int)*((*__n)+1));
	for(i = 0; i < *__ne; i++)
	{
		if(J[i] < 0)
		{
			printf("col index <  0\n");
			exit(16);
		}
		if(J[i] >= *__n)
		{
			printf("col index >= n\n");
			exit(17);
		}
		wrk[J[i]]++;
	}

	lcsc_jcn[0] = 0;
	for(i = 1; i<= *__n; i++)
	{
		lcsc_jcn[i] = (unsigned int) wrk[i-1];
		wrk[i] = wrk[i-1] + wrk[i];
	}

	if(wrk[*__n] != *__ne)
	{
		printf("numbers do not add up\n");
		exit(18);
	}

	for(i = *__ne-1; i>=0; i--)
	{
		int rn = I[i], cn = J[i];
		if(rn < 0)   
		{
			printf("row index <  0\n");
			exit(19);
		}
		if(rn >= *__m) 
		{
			printf("row index >=  m\n"); 
			exit(20);
		}  
		lcsc_irn[--(wrk[cn])] = (unsigned int) rn;		
		

	}
	for(i=0; i<=*__n; i++)
	{
		if(lcsc_jcn[i] != wrk[i]) 
		{
			printf("arrays do not line up\n");
			exit(21);
		}
	}
	free(I);
	free(J);
	if( val )
		free(val);
	free(wrk);

	return ;
}
void printMatrix(unsigned int *ptrs, unsigned int *vids, unsigned int n)
{
	unsigned int i, j;
	for (i=0 ; i < n; i++)
	{	
		for (j = ptrs[i]; j < ptrs[i+1]; j++)
		{
			printf("%d ", vids[j]);
		}
		printf("\n");
	}
}
void discardDiag(unsigned int *ptrs, unsigned int *vids, unsigned int n, unsigned int *ne)
{
	unsigned int i, j;
	unsigned int off = 0;
	for (i=0 ; i < n; i++)
	{	
		unsigned int st = off;
		for (j = ptrs[i]; j < ptrs[i+1]; j++)
		{
			if (vids[j] != i)
			{
				vids[off++] = vids[j];
			}	
		}
		ptrs[i] = st;
	}
	*ne = ptrs[n] = off;
}
int smallInstanceTest()
{
	unsigned int *ptrs, *vids;
	unsigned int n = 8;
	unsigned int  ne = 13;
	unsigned int *perm = (unsigned int *) malloc(sizeof(unsigned int) * n);
	unsigned int *cptrs = (unsigned int *) malloc(sizeof(unsigned int) * (n+1));
	int *lowl = (int *) malloc(sizeof(int) * (n+1));
	int *spos = (int *) malloc(sizeof(int) * (n+1));
	unsigned int *prev = (unsigned int *) malloc(sizeof(unsigned int) * n);
	unsigned int *tedges = (unsigned int *) malloc(sizeof(unsigned int) * (n+1));
	unsigned int nscc;
	ptrs = (unsigned int *) malloc(sizeof(unsigned int) * (n+1));
	vids = (unsigned int *) malloc(sizeof(unsigned int) * ne);
	
	ptrs[0] = 0;   vids[0] = 1;
	ptrs[1] = 1;   vids[1] = 2; vids[2] = 4; vids[3] = 5; 
	ptrs[2] = 4;   vids[4] = 3; vids[5] = 6; 
	ptrs[3] = 6;   vids[6] = 2; vids[7] = 7; 
	ptrs[4] = 8;   vids[8] = 0; vids[9] = 5; 
	ptrs[5] = 10;  vids[10] = 6;
	ptrs[6] = 11;  vids[11] = 7; vids[12] =  5;
	ptrs[7] = 13;
	ptrs[8] = ne;

	nscc = scc(ptrs, vids,  n, perm, cptrs, lowl,  spos, prev,  tedges) ;
	printf("the num scc %d\n", nscc);

	if(nscc < 10 && n < 10)
	{
		for (unsigned int i = 0; i < nscc; i++)
		{
			for (unsigned j = cptrs[i]; j < cptrs[i+1]; j++)
			{
				printf("%d ", perm[j]);
			}
			printf("\n");
		}
	}
	free(vids);
	free(ptrs);
	free(tedges);
	free(prev);
	free(spos); 
	free(lowl); 
	free(cptrs); 
	free(perm);	

	return 0;
}

int main(int argc, char *argv[])
{
	unsigned int *ptrs, *vids;
	unsigned int n, m ;
	unsigned int ne;

	unsigned int *perm ;
	unsigned int *cptrs ;
	int *lowl ;
	int *spos ;
	unsigned int nscc;
	unsigned int *prev ;
	unsigned int *tedges ;
	if(argc != 2)
	{
		printf("%s <filename>\n", argv[0]);
		return 1;
	}
	ReadMatrixMarket(argv[1], & vids, &ptrs, &m,&n, &ne, 0);
	printf("read in vals %u %u %u\n", m, n, ne);
	if( m != n)
	{
		printf("neeed square matrix\n");
		return 1;
	}

	discardDiag(ptrs, vids, n, &ne);
	printf("new ne %u\n", ne);

	perm = (unsigned int *) malloc(sizeof(unsigned int) * n);
	cptrs = (unsigned int *) malloc(sizeof(unsigned int) * (n+1));
	lowl = (int *) malloc(sizeof(int) * (n+1));
	spos = (int *) malloc(sizeof(int) * (n+1));
	prev = (unsigned int *) malloc(sizeof(unsigned int) * n);
	tedges = (unsigned int *) malloc(sizeof(unsigned int) * (n+1));
	nscc = scc(ptrs, vids,  n, perm, cptrs, lowl,  spos, prev,  tedges) ;
	printf("the num scc %d\n", nscc);
	free(vids);
	free(ptrs);
	free(tedges);
	free(prev);
	free(spos); 
	free(lowl); 
	free(cptrs); 
	free(perm);	

	return 0;


}
