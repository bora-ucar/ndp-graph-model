CC = gcc
CFLAGS = -O3 -Wall

all: exps 

%.o: %.c
	$(CC) -c $< $(CFLAGS)

exps: exps.o hscc.o randHedges.o
	$(CC) $^ -o $@ $(CFLAGS) -lm

testSCC: testscc.o hscc.o mmio.o
	$(CC) $^ -o $@ $(CFLAGS) -lm
clean:
	rm -f *.o exps testSCC
