#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>


void setDimensions(unsigned int *dimensions, unsigned int d, unsigned int minSz, unsigned int maxSz)
{
	for (unsigned int i = 0; i < d; i++)
		dimensions[i] = (rand()% (maxSz - minSz)) + minSz;/*between minsz and maxSz - 1*/
}

unsigned int _global_d;
int comp_hedge_array(const void *h1,const void*h2)
{
	unsigned int *h1p = (unsigned int*) h1;
	unsigned int *h2p = (unsigned int*) h2;
	for (unsigned int i = 0; i < _global_d; i++)
	{
		if (h1p[i] < h2p[i]) return -1;
		if (h1p[i] > h2p[i]) return 1;
	}
	return 0;
}
void createEdges(unsigned int *  dimensions,  unsigned int  d,  unsigned int  nhedges, unsigned int *hedges)
{
	for (unsigned int j = 0; j < nhedges; j++)
	{
		for (unsigned int t = 0; t < d; t++)
		{
			hedges[j*d+t]=rand()%dimensions[t]; 
		}
	}
}

void addToList(unsigned int **hedges, unsigned int *nhedges, unsigned int *annz, unsigned int d, unsigned int *allocSz)
{


	unsigned int t, st = *nhedges * d;

	if (*nhedges == *allocSz)
	{
		*allocSz = ceil( 1.5 * (*allocSz )); /*we will surely be wasting but no big deal*/
		(*hedges) = (unsigned int *) realloc(*hedges, sizeof(unsigned int) *d*(*allocSz));	
		printf("realloc occurs\n");
	}
		
	for (t = 0; t < d; t++, st++)		
		(*hedges)[st] = annz[t];			
	*nhedges = *nhedges + 1;

}

void uniformNnz(unsigned int **hedges, unsigned int *nhedges, unsigned int d, unsigned int* dimensions, double p)
{
/*
given d (num dimensions), and theis sizes, fills *hedges, and returns their number.
This will loop over all nonzeros, so n^d should not be too big.
*/
	double npowd;
	unsigned int t, allocSz ;
	npowd = 1.0;
	for (t = 0; t < d; t++)
		npowd = npowd * dimensions[t];

	if(npowd>=UINT_MAX)
	{
		printf("prod(dims) is too big : number of nonzeros to consider\n");
		exit(1);
	}
	if(p == 1.0) /*dense*/
		allocSz	= (unsigned int) ceil (npowd);
	else
		allocSz	= (unsigned int) ceil (1.5 * p * npowd);/*we will surely be wasting but no big deal*/

	(*hedges) = (unsigned int *) malloc(sizeof(unsigned int) * (allocSz*d) ); 

	unsigned int *nextCoord = (unsigned int*) malloc(sizeof(unsigned int) *d);
	unsigned int carryIn  ;

	for (t = 0; t < d; t++)
		nextCoord[t] = 0;

	carryIn=0; 
	*nhedges = 0;
	while(carryIn == 0)/*out from the last significant bit*/
	{
		double rv =  (double)rand() / (double)RAND_MAX ;

		if(rv <= p) /*add to the list of nonzeros*/		
			addToList(hedges, nhedges, nextCoord,  d, &allocSz);

		carryIn = 1;
		for (t = 0; t < d && carryIn == 1; t++)
		{
			if (nextCoord[t] == dimensions[t] - 1)			 					 		
		 	{
		 		nextCoord[t] = 0;
		 		carryIn = 1;
		 	}
		 	else
		 	{
				nextCoord[t] = nextCoord[t] + carryIn;
				carryIn = 0;
		 	}
		}	
	}

	free(nextCoord);
}

void setHyperedges(unsigned int *hedges, unsigned int *nhedges, unsigned int d, unsigned int *dimensions, int checkDuplicates)
{
	unsigned int * marker;  
	unsigned int i, j;

	createEdges(dimensions, d, *nhedges, hedges);
	if( checkDuplicates )/*if no duplicate hedges are allowed, then sort and get rid of the duplicates*/
	{		
		marker = (unsigned int*) malloc(sizeof(unsigned int) * *nhedges);
		_global_d = d;/*we can discard duplicated hedges by sorting as seen below; or with other methods.*/
		qsort(hedges, *nhedges, d * sizeof(unsigned int), comp_hedge_array) ;
		for(i = 0; i < *nhedges; i++)
			marker[i] = 1;

		for(i = 1; i < *nhedges; i++)
		{
			if (comp_hedge_array(&(hedges[(i-1)*d]) , &(hedges[i*d]))  == 0)
				marker[i] = 0;
		}

		j = 0;
		for (i = 0; i < *nhedges; i++)
		{
			if(marker[i])
			{
				for (unsigned int t = 0; t < d; t++)
					hedges[j*d + t] = hedges[i*d + t];
				j++;
			}
		}
		*nhedges = j;
		free(marker);
	}
}