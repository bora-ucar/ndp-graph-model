# Connectivity of a random directed graph model
This software creares D(n,d,p) random directed graphs as described in the technical report,
and tests their strong connectivity. It does so by creating sparse random tensors, which 
can be of interest.

**Anne Benoit, Kamer Kaya, and Bora Uçar**, *Connectivity of a random directed graph model*, [Research Report] RR-9540, Inria Grenoble Rhône-Alpes. 2024.
[⟨hal-04428417⟩](https://inria.hal.science/hal-04428417).

<pre>
@techreport{benoit:hal-04428417,
  TITLE = {{Connectivity of a random directed graph model}},
  AUTHOR = {Benoit, Anne and Kaya, Kamer and U{\c c}ar, Bora},
  URL = {https://inria.hal.science/hal-04428417},
  NUMBER = {RR-9540},
  INSTITUTION = {{Inria Lyon}},
  YEAR = {2024},
  MONTH = Jan,
  KEYWORDS = {Random graphs ; strong connectivity ; sparse tensor},
  HAL_ID = {hal-04428417},
  HAL_VERSION = {v1},
}
</pre>

