#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <limits.h>
#include <math.h>
#include <time.h>


unsigned int sccHypergraphs(unsigned int *hedges, unsigned int nhedges, unsigned int d, unsigned int *dimensions, unsigned int *nedgesGraph);
void setHyperedges(unsigned int *hedges, unsigned int *nhedges, unsigned int d, unsigned int *dimensions, int checkdublicates);
void uniformNnz(unsigned int **_hedges, unsigned int *nhedges, unsigned int d, unsigned int* dimensions, double p);


void printUsage(char *execName) 
{
	printf("Usage : \n\t %s\n", execName);
	printf("\t--numDims <d>    : the number of dimensions\n");
	printf("\t--sizeDim <n>    : the size in each dim\n");
	printf("\t--cnstnt <c>     : the constant per n\n");
	printf("\t[--model <m>]    :  binomial model (0 default) or uniform random tns model (1)\n");

	printf("\t[--numRuns <r>]  : the number of repetitions (optional, default 1000)\n");
	printf("\t[--linSpace <l>  : the linear space from -2 to c (optional, default 101). If l=1, then only c is tested\n");
	printf("\t\t\tgive -l 1 for a single value (=c); odd number otherwise\n");
	
	printf("\t--help <h>       : help\n");
}

int parseArguments(int argc, char *argv[],
	unsigned int *d, unsigned int *n, unsigned int *_c,  unsigned int *_l,
	unsigned int *numTests,
	unsigned int *model)
{
	*numTests = 1000;
	*d = *n = *_c = 0;
	*model = 0;
	*_l = 101;
	static char* const short_opts = "d:n:r:m:c:l:h";
	static struct option long_opts[] = {
		{"numDims", required_argument, 0, 'd'},
		{"sizeDim", required_argument, 0, 'n'},
		{"cnstnt", required_argument,  0, 'c'},
		{"model", optional_argument, 0, 'm'},
		{"linSpace", optional_argument, 0, 'l'},
		{"numRuns", optional_argument, 0, 'r'},
		{"help", no_argument, 0, 'h'},
		{0, 0, 0, 0}
	};

	while(1)
	{
		int c, option_index;
		c = getopt_long(argc, argv, short_opts,	long_opts, &option_index);
		if (c == -1)
			break;
		switch(c)
		{	
		case 'd' :
			*d = (unsigned int) atoi(optarg);
			break;
		case 'n' :
			*n = (unsigned int) atoi(optarg);
			break;
		case 'c' :
			*_c = (unsigned int) atoi(optarg);
			break;
		case 'l' :
			*_l = (unsigned int) atoi(optarg);
			break;
		case 'r' :
			*numTests = (unsigned int) atoi(optarg);
			break;
		case 'm' :
			*model = (unsigned int) atoi(optarg);
			break;
				
		case 'h' :
		case '?' :
		default:
			printUsage(argv[0]);
			return 1;
		}	
	}
	if (*n<=2 || *_c < 1)
	{
		printf("sizes are not specified/OK ");
		printUsage(argv[0]);
		return 1;
	}
	
	if(*numTests < 1)
	{
		printf("numTests is not OK (need >= 1) ");
		printUsage(argv[0]);
		return 1;
	}
	if(log(*n) <= *_c)
	{
		printf("log n and c are not OK (need log n > c)");
		printUsage(argv[0]);
		return 1;

	}
	if(*_l<1)
	{
		printf("linSpace is not ok (need >=1 and odd)");
		printUsage(argv[0]);
		return 1;	
	}
	if(*model != 0  && *model != 1)
	{
		printf("model is not ok (need 0 or 1)");
		printUsage(argv[0]);
		return 1;	
	}
		
	if (*_l% 2 == 0)
		*_l = *_l + 1;	

	return  0;
}

double testOneSetup(unsigned int n, unsigned d, double nhedges_d, unsigned int numTests, unsigned *nedgesGraph)
{
	unsigned int *dimensions;
	unsigned int *numscc , i;
	unsigned int singleComps;
	double numEddgesGraphs = 0;

	if ( nhedges_d>= UINT_MAX)
	{
		printf("hedges too big\n");
		exit(1);
	}
	
	dimensions = (unsigned int *) malloc(sizeof(unsigned int) * d);
	numscc = (unsigned int *) malloc(sizeof(unsigned int) * numTests);

	for (unsigned int i = 0; i < d; i++)
		dimensions[i]  = n;
	for (i = 0; i < numTests; i++)
	{
		unsigned int anumEdge;
		unsigned int nhedges = (unsigned int) ceil(nhedges_d);
		unsigned int *hedges = (unsigned int *) malloc(sizeof(unsigned int) * (nhedges * d));

		setHyperedges(hedges, &nhedges, d, dimensions, 0); /*we do not need to get rid of the duplicate hedges; 
															 construction of the directed graph handles it*/
		
		numscc[i] = sccHypergraphs(hedges, nhedges, d, dimensions, &anumEdge);
		numEddgesGraphs += anumEdge;

		free(hedges);
	}
	singleComps = 0;
	for (i = 0; i < numTests; i++)
		singleComps += numscc[i] == 1;

	free(numscc);
	free(dimensions);
	*nedgesGraph = (unsigned int) ceil(numEddgesGraphs/numTests);
	return ((double)singleComps) / ((double)numTests);
}

void fillLinSpace(double *nhedges_d, int sz, unsigned int n, unsigned int d, unsigned int c, unsigned int model)
{
	if(model == 0)
	{	
/*from 0 to c in sz steps*/

/*in case we want to look some <= steps	double init = (double)n * log((double) n)  - 1.0 * (double) c * n; */
		double init = (double)n * log((double) n) - 2.0 * n; 

		if(sz > 1)
		{
			double step = (c+2.0) * (double) n /((double) sz-1.0);/*from -2.0 to c*/
			for (int i = 0; i < sz; i++)
				nhedges_d[i] = floor(init + step * i); /*n log n+c*/
			
		}
		else
		{
			nhedges_d[0]  = (double)n * log((double) n) + c * (double) n ;
		}
	}
	else
	{
		double npowd1;
		npowd1 = 1.0;
		for (int t = 0; t < d-1; t++)
			npowd1 = npowd1 * n;

		if(sz > 1)
		{
			double step = (c+2.0) /((double) sz-1.0); /*from -2 to c*/

			for (int i = 0; i < sz; i++)
				nhedges_d[i] = (log((double)n) -2.0 + step*i) / npowd1;
		}
		else
			nhedges_d[0]  = (log((double)n) + c) / npowd1;

	}
}

double testUniform( unsigned int n, unsigned int d, unsigned int numTests, unsigned *nedgesGraph, double p)
{
	unsigned int *hedges;
	unsigned int nhedges ;
	unsigned int *dimensions= (unsigned int *) malloc(sizeof(unsigned int)*d);
	unsigned int t;
	double numEdgesGraphs = 0;
	unsigned int *numscc , i;
	unsigned int singleComps;

	if (d * log((double) n) > 4 * log((double) 50.0))
	{
		printf("\n====================== Warning ======================\n");
		printf("\tThis will take long.\n");
		printf("=====================================================\n");
	}
	numscc = (unsigned int *) malloc(sizeof(unsigned int) * numTests);
	for(t = 0; t < d; t++)
		dimensions[t] = n;

	for (i = 0; i < numTests; i++)
	{
		unsigned int nedgesGraphi;
		uniformNnz(&hedges, &nhedges,  d, dimensions, p);
		numscc[i] = sccHypergraphs(hedges, nhedges, d, dimensions, &nedgesGraphi);
		numEdgesGraphs += nedgesGraphi;
		free(hedges);
	}
	*nedgesGraph = (unsigned int) ceil(numEdgesGraphs/numTests);
	
	singleComps = 0;
	for (i = 0; i < numTests; i++)
		singleComps += numscc[i] == 1;

	free(numscc);
	free(dimensions);

	return ((double)singleComps) / ((double)numTests);
}

int main(int argc, char *argv[])
{

	unsigned int d, n, c,  numTests, model;
	unsigned int lsSize ;
	double proba;
	double *nhedges_dp;

	int err = parseArguments(argc, argv, &d, &n, &c, &lsSize, &numTests, &model);
	if (err) exit(err);

	srand(time(NULL));

	nhedges_dp = (double *) malloc(sizeof(double) * (lsSize));

	fillLinSpace(nhedges_dp, lsSize, n, d, c, model);
	for (unsigned int ls = 0; ls < lsSize; ls++)
	{		
		unsigned int nedgesGraph;
		if( model == 0)
		{
			proba = testOneSetup(n, d,  nhedges_dp[ls], numTests, &nedgesGraph);
			printf("%d %.2f %d %d %.0f %.12f %d\n", ls+1, (nhedges_dp[ls] - floor((double)n * log((double) n) ))/((double) n), n, d, nhedges_dp[ls], proba, nedgesGraph);
		}
		else
		{	
			proba =  testUniform( n,  d,  numTests, &nedgesGraph, nhedges_dp[ls]);
			printf("%d %.2f %d %d %.12f %d\n", ls+1, ((nhedges_dp[ls] * pow((double)n,(double)d))  -  (double)n * log((double) n))/((double) n), n, d, proba, nedgesGraph);
		}
	}
	free(nhedges_dp);
	return 0;
}