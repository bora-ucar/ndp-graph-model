## For Fig 2:
./exps -d 4 -c 4 -n 100 -m 0 -l 151 -r 100000 > outputRuns_d4_c_2_4_n100.txt &
./exps -d 4 -c 4 -n 1000 -m 0 -l 151 -r 100000 >outputRuns_d4_c_2_4_n1000.txt &


## additional For fig 3:
./exps -d 2 -c 4 -n 100 -m 0 -l 151 -r 100000 > outputRuns_d2_c_2_4_n100.txt &
./exps -d 8 -c 4 -n 100 -m 0  -l 151 -r 100000 > outputRuns_d8_c_2_4_n100.txt &
./exps -d 16 -c 4 -n 100 -m 0 -l 151 -r 100000 > outputRuns_d16_c_2_4_n100.txt &


## for the n=50 case
 ./exps -d 2 -c 3 -n 50 -m 0 -l 151 -r 100000 > outputRuns_d2_c_2_3_n50.txt &
 ./exps -d 2 -c 3 -n 50 -m 0 -l 151 -r 100000 > outputRuns_d3_c_2_3_n50.txt &
 ./exps -d 4 -c 3 -n 50 -m 0 -l 151 -r 100000 > outputRuns_d4_c_2_3_n50.txt &
 ./exps -d 8 -c 3 -n 50 -m 0 -l 151 -r 100000 > outputRuns_d8_c_2_3_n50.txt &
 ./exps -d 16 -c 3 -n 50 -m 0 -l 151 -r 100000 > outputRuns_d16_c_2_3_n50.txt &

## uniform case
./exps -d 3 -c 3 -n 50 -m 1 -l 101 -r r 10000 > outputRuns_d3_c3_n50_model1.txt	&
./exps -d 4 -c 3 -n 50 -m 1 -l 101 -r r 10000 >  outputRuns_d4_c3_n50_model1.txt &

